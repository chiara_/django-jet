FROM ubuntu:16.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install --fix-missing -y \
    build-essential \
    curl \
    gcc \
    git \
    libpq-dev \
    libsqlite3-dev \
    ghostscript \
    make \
    wget \
    gettext

COPY package.json /app/package.json
WORKDIR /app

# node v8 required
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs

# python and django
RUN cd /opt && wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz && \
    tar -xvf Python-3.6.3.tgz && cd Python-3.6.3 && ./configure && \
    make && make install && \
    pip3 install -U pip setuptools && \
    pip install --upgrade pip && \
    pip install django==2.1

RUN ln -sfn /usr/local/bin/python3.6 /usr/bin/python

RUN npm install
RUN npm install --global gulp-cli

COPY . /app


